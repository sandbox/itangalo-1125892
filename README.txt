This module provides a small CTools plugin that allows you to set the breadcrumb in custom pages created by Page manager. You can use CTools replacement patterns to do this.

If replacement patterns result in a breadcrumb part being repeated (such as top-level term > current term), the duplicates are pruned.

This functionality is implemented as an <em>access plugin</em>, meaning that you configure it while setting up access rules, variant selection or visibility rules. It is far from ideal to implement it this way, but it was the best way I found at the moment. (I have now learned that it is perfectly possible to add new classes of configuration to custom pages, such as actions to run, but hey.)

Patches, ideas spinoffs welcome.