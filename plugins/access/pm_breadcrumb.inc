<?php
/**
 * @file
 * Plugin to set a breadcrumb while checking rules for access, selecting
 * variants of deciding visibility. Yeah, this isn't the way to do it, but it
 * is ONE way of doing it if you can't find a way of implementing custom
 * PHP functions as its own hook in CTools.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Set breadcrumb'),
  'description' => t('Set the breadcrumb. Does not affect access in any way.'),
  'callback' => 'pm_breadcrumb_set_breadcrumb',
  'settings form' => 'pm_breadcrumb_breadcrumb_settings',
  'summary' => 'pm_breadcrumb_breadcrumb_summary',
  'all contexts' => TRUE,
);

/**
 * Breadcrumb settings
 */
function pm_breadcrumb_breadcrumb_settings(&$form, &$form_state, $conf) {
  $form['settings']['warning'] = array(
    '#value' => t('Note that executing this access check will set the breadcrumb,
      even if other access checks aborts further execution. Thus, it is wise to
      put this hack of a access rule last of all rules, or even in the variant
      selection.'),
  );
  $form['settings']['titles'] = array(
    '#type' => 'textarea',
    '#title' => t('Breadcrumb titles'),
    '#description' => t('Enter one title per line. Front page is automatically
      prepended, by Drupal standard. Any duplicate breadcrumbs are removed.'),
    '#default_value' => $conf['titles'],
  );
  $form['settings']['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Breadcrumb paths'),
    '#description' => t('Enter one path per line. Front page is automatically
      prepended, by Drupal standard. Any duplicate breadcrumbs are removed.'),
    '#default_value' => $conf['paths'],
  );
  $form['settings']['substitute'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use context keywords'),
    '#description' => t('It is possible to use keyword substitutions on the form
      %node:title or %account:user-name. Actual keywords depend on the existing
      context.'),
    '#default_value' => $conf['substitute'],
  );
}

/**
 * Set the breadcrumb
 */
function pm_breadcrumb_set_breadcrumb($conf, $context , $tmp) {
  // Replace any existing keywords, if substitution is set
  if ($conf['substitute']) {
    $conf['titles'] = ctools_context_keyword_substitute($conf['titles'], array(), $context);
    $conf['paths'] = ctools_context_keyword_substitute($conf['paths'], array(), $context);
  }

  $titles = explode("\n", $conf['titles']);
  $paths = explode("\n", $conf['paths']);

  // Build all breadcrumbs
  $breadcrumbs = array(l(t('Home'), '<front>'));
  foreach ($titles as $key => $title) {
    // Only add a breadcrumb if we both have a title and a path
    if ($title && $paths[$key]) {
      $breadcrumbs[] = l(t($title), $paths[$key]);
    }
  }

  // Remove any duplicate breadcrumbs
  $breadcrumbs = array_unique($breadcrumbs);

  // Set breadcrumbs, then always return TRUE to not affect access
  drupal_set_breadcrumb($breadcrumbs);
  return TRUE;
}

/**
 * Provide a summary of this access setting
 */
function pm_breadcrumb_breadcrumb_summary($conf, $context) {
  if ($conf['substitute']) {
    return t('Sets breadcrumbs for this page using context keywords. Does not affect access.');
  }

  return t('Sets breadcrumbs for this page. Does not affect access.');
}
